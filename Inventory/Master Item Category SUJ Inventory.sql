-- Master Category in category set: "SUJ_Inventory_Category"
 SELECT
   msi.segment1 item_code
 , msi.inventory_item_id
 , mcs.category_set_name
 , mc.CONCATENATED_SEGMENTS category_name
 , mic.creation_date
 , mic.last_update_date
   FROM
   mtl_item_categories mic
 , mtl_categories_kfv mc
 , mtl_category_sets mcs
 , mtl_system_items msi
  WHERE
   mcs.category_set_id = mic.category_set_id
   AND msi.organization_id = mic.organization_id
   AND msi.inventory_item_id = mic.inventory_item_id
   AND mic.category_id = mc.category_id
   AND mcs.category_set_name = 'SUJ_Inventory_Category'
   AND EXISTS
   (SELECT
      1
      FROM
      fnd_profile_option_values pv
    , fnd_profile_options po
     WHERE
      po.profile_option_id = pv.profile_option_id
      AND po.profile_option_name = 'QP_ORGANIZATION_ID'
      AND level_id = 10001
      AND mic.organization_id = pv.profile_option_value
   ) ;