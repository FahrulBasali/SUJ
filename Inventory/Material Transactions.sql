select mmt.transaction_id, mmt.transaction_source_name,  mp.organization_code, msi.segment1, mtt.transaction_type_name, flv.meaning transaction_action, mtxs.transaction_source_type_name,mmt.transaction_quantity, mmt.primary_quantity, mmt.transaction_uom, mmt.transaction_date, mmt.transaction_cost, mmt.subinventory_code, mil.concatenated_segments locator,
mmt.attribute_category, mmt.attribute1, mmt.attribute2, mmt.attribute3, mmt.attribute4, mmt.attribute5, mmt.attribute6, mmt.attribute7, mmt.attribute8,mmt.attribute9,mmt.attribute10,mmt.attribute11,mmt.attribute12,mmt.attribute13,mmt.attribute14,mmt.attribute15, mmt.creation_date, mmt.last_update_date
 from mtl_material_transactions mmt, mtl_parameters mp, mtl_transaction_types mtt,
  mtl_system_items msi, mtl_item_locations_kfv mil, fnd_lookup_values flv,  MTL_TXN_SOURCE_TYPES mtxs
  where mmt.organization_id = mp.organization_id
and mtt.transaction_type_id = mmt.transaction_type_id
and mmt.organization_id = msi.organization_id
and msi.inventory_item_id =mmt.inventory_item_id
and mil.inventory_location_id (+) = mmt.locator_id
and flv.lookup_type = 'MTL_TRANSACTION_ACTION'
and flv.lookup_code = mtt.TRANSACTION_ACTION_ID
and mtxs.TRANSACTION_SOURCE_TYPE_ID = mmt.TRANSACTION_SOURCE_TYPE_ID
--and mil.organization_id = mmt.organization_id
