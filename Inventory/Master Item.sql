SELECT segment1 item_code, description ,inventory_item_id item_id, primary_uom_code unit_of_measure, item_type, inventory_item_flag inventory_item,
       inventory_item_status_code, purchasing_enabled_flag purchasable, customer_order_flag orderable, costing_enabled_flag costed, creation_date, last_update_date
  FROM mtl_system_items
 WHERE organization_id = (SELECT profile_option_value
                            FROM fnd_profile_option_values pv, fnd_profile_options po
                           WHERE po.profile_option_id = pv.profile_option_id AND po.profile_option_name = 'QP_ORGANIZATION_ID' AND level_id = 10001)
