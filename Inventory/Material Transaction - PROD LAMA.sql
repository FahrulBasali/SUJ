--Prod Lama - FG
SELECT mmt.transaction_id
     , mmt.transaction_source_name
     , mp.organization_code
     , msi.segment1
     , mtt.transaction_type_name
     , flv.meaning transaction_action
     , mtxs.transaction_source_type_name
     , mmt.transaction_source_id
     , mmt.transaction_quantity
     , mmt.primary_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , CASE WHEN uc.amount IS NOT NULL THEN uc.amount * SIGN ( mmt.transaction_quantity) ELSE mmt.actual_cost * mmt.transaction_quantity END amount
     , mmt.subinventory_code
     , mil.concatenated_segments locator
     , mmt.attribute_category
     , mmt.attribute1
     , mmt.attribute2
     , mmt.attribute3
     , mmt.attribute4
     , mmt.attribute5
     , mmt.attribute6
     , mmt.attribute7
     , mmt.attribute8
     , mmt.attribute9
     , mmt.attribute10
     , mmt.attribute11
     , mmt.attribute12
     , mmt.attribute13
     , mmt.attribute14
     , mmt.attribute15
     , mmt.creation_date
     , mmt.last_update_date
     , DECODE (mmt.logical_transaction, 1, 'Y', 'N') Logical_transactions
  FROM mtl_material_transactions mmt
     , mtl_parameters mp
     , mtl_transaction_types mtt
     , mtl_system_items_kfv msi
     , mtl_item_locations_kfv mil
     , fnd_lookup_values flv
     , mtl_txn_source_types mtxs
     , (  SELECT cd.transaction_id, ABS ( SUM ( cd.layer_quantity * NVL (slc.layer_cost, cd.actual_cost))) amount
            FROM mtl_cst_layer_act_cost_details cd, suj_fg_layer_cost slc
           WHERE cd.inv_layer_id = slc.inv_layer_id(+)
        GROUP BY transaction_id) uc
 WHERE     mmt.organization_id = mp.organization_id
       AND mtt.transaction_type_id = mmt.transaction_type_id
       AND mmt.organization_id = msi.organization_id
       AND msi.inventory_item_id = mmt.inventory_item_id
       AND mil.inventory_location_id(+) = mmt.locator_id
       AND flv.lookup_type = 'MTL_TRANSACTION_ACTION'
       AND flv.lookup_code = mtt.transaction_action_id
       AND mtxs.transaction_source_type_id = mmt.transaction_source_type_id
       AND msi.concatenated_segments LIKE '1-FG%'
       AND CASE WHEN mmt.transaction_action_id IN (2, 28) AND SIGN ( mmt.transaction_quantity) = 1 THEN mmt.transfer_transaction_id ELSE mmt.transaction_Id END =
              uc.transaction_id(+)
UNION ALL
--Prod Lama dan non-FG
SELECT mmt.transaction_id
     , mmt.transaction_source_name
     , mp.organization_code
     , msi.segment1
     , mtt.transaction_type_name
     , flv.meaning transaction_action
     , mtxs.transaction_source_type_name
     , mmt.transaction_source_id
     , mmt.transaction_quantity
     , mmt.primary_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , CASE WHEN uc.amount IS NOT NULL THEN uc.amount * SIGN ( mmt.transaction_quantity) ELSE mmt.actual_cost * mmt.transaction_quantity END amount
     , mmt.subinventory_code
     , mil.concatenated_segments locator
     , mmt.attribute_category
     , mmt.attribute1
     , mmt.attribute2
     , mmt.attribute3
     , mmt.attribute4
     , mmt.attribute5
     , mmt.attribute6
     , mmt.attribute7
     , mmt.attribute8
     , mmt.attribute9
     , mmt.attribute10
     , mmt.attribute11
     , mmt.attribute12
     , mmt.attribute13
     , mmt.attribute14
     , mmt.attribute15
     , mmt.creation_date
     , mmt.last_update_date
     , DECODE (mmt.logical_transaction, 1, 'Y', 'N') Logical_transactions
  FROM mtl_material_transactions mmt
     , mtl_parameters mp
     , mtl_transaction_types mtt
     , mtl_system_items_kfv msi
     , mtl_item_locations_kfv mil
     , fnd_lookup_values flv
     , mtl_txn_source_types mtxs
     , (  SELECT transaction_id, ABS ( SUM ( layer_quantity * actual_cost)) amount
            FROM mtl_cst_layer_act_cost_details
        GROUP BY transaction_id) uc
 WHERE     mmt.organization_id = mp.organization_id
       AND mtt.transaction_type_id = mmt.transaction_type_id
       AND mmt.organization_id = msi.organization_id
       AND msi.inventory_item_id = mmt.inventory_item_id
       AND mil.inventory_location_id(+) = mmt.locator_id
       AND flv.lookup_type = 'MTL_TRANSACTION_ACTION'
       AND flv.lookup_code = mtt.transaction_action_id
       AND mtxs.transaction_source_type_id = mmt.transaction_source_type_id
       AND msi.concatenated_segments NOT LIKE '1-FG%'
       AND CASE WHEN mmt.transaction_action_id IN (2, 28) AND SIGN ( mmt.transaction_quantity) = 1 THEN mmt.transfer_transaction_id ELSE mmt.transaction_Id END =
              uc.transaction_id(+)