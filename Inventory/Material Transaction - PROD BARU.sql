--baru dan non-FG
SELECT mmt.transaction_id
     , mmt.transaction_source_name
     , mp.organization_code
     , msi.segment1
     , mtt.transaction_type_name
     , flv.meaning transaction_action
     , mtxs.transaction_source_type_name
     , mmt.transaction_source_id
     , mmt.transaction_quantity
     , mmt.primary_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , NVL (mmt.actual_cost, mmt.transaction_cost) * mmt.transaction_quantity amount
     , mmt.subinventory_code
     , mil.concatenated_segments locator
     , mmt.attribute_category
     , mmt.attribute1
     , mmt.attribute2
     , mmt.attribute3
     , mmt.attribute4
     , mmt.attribute5
     , mmt.attribute6
     , mmt.attribute7
     , mmt.attribute8
     , mmt.attribute9
     , mmt.attribute10
     , mmt.attribute11
     , mmt.attribute12
     , mmt.attribute13
     , mmt.attribute14
     , mmt.attribute15
     , mmt.creation_date
     , mmt.last_update_date
     , DECODE (mmt.logical_transaction, 1, 'Y', 'N') Logical_transactions
  FROM mtl_material_transactions mmt
     , mtl_parameters mp
     , mtl_transaction_types mtt
     , mtl_system_items_kfv msi
     , mtl_item_locations_kfv mil
     , fnd_lookup_values flv
     , mtl_txn_source_types mtxs
 WHERE     mmt.organization_id = mp.organization_id
       AND mtt.transaction_type_id = mmt.transaction_type_id
       AND mmt.organization_id = msi.organization_id
       AND msi.inventory_item_id = mmt.inventory_item_id
       AND mil.inventory_location_id(+) = mmt.locator_id
       AND flv.lookup_type = 'MTL_TRANSACTION_ACTION'
       AND flv.lookup_code = mtt.transaction_action_id
       AND mtxs.transaction_source_type_id = mmt.transaction_source_type_id
       AND msi.concatenated_segments NOT LIKE '1-FG%'
UNION ALL
--baru FG
SELECT mmt.transaction_id
     , mmt.transaction_source_name
     , mp.organization_code
     , msi.segment1
     , mtt.transaction_type_name
     , flv.meaning transaction_action
     , mtxs.transaction_source_type_name
     , mmt.transaction_source_id
     , mmt.transaction_quantity
     , mmt.primary_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , cog.cogs_unit_cost * mmt.transaction_quantity amount
     , mmt.subinventory_code
     , mil.concatenated_segments locator
     , mmt.attribute_category
     , mmt.attribute1
     , mmt.attribute2
     , mmt.attribute3
     , mmt.attribute4
     , mmt.attribute5
     , mmt.attribute6
     , mmt.attribute7
     , mmt.attribute8
     , mmt.attribute9
     , mmt.attribute10
     , mmt.attribute11
     , mmt.attribute12
     , mmt.attribute13
     , mmt.attribute14
     , mmt.attribute15
     , mmt.creation_date
     , mmt.last_update_date
     , DECODE (mmt.logical_transaction, 1, 'Y', 'N') Logical_transactions
  FROM mtl_material_transactions mmt
     , mtl_parameters mp
     , mtl_transaction_types mtt
     , mtl_system_items msi
     , mtl_item_locations_kfv mil
     , fnd_lookup_values flv
     , mtl_txn_source_types mtxs
     , (SELECT cog.cogs_unit_cost
             , cog.periode_name
             , cog.organization_id
             , msi_cog.concatenated_segments item_code
             , msi_cog.inventory_item_id
          FROM suj_fg_cogm_cogs_hist cog, mtl_system_items_kfv msi_cog
         WHERE msi_cog.concatenated_segments = cog.item_code AND msi_cog.organization_id = cog.organization_id) cog
 WHERE     mmt.organization_id = mp.organization_id
       AND mtt.transaction_type_id = mmt.transaction_type_id
       AND mmt.organization_id = msi.organization_id
       AND msi.inventory_item_id = mmt.inventory_item_id
       AND mil.inventory_location_id(+) = mmt.locator_id
       AND flv.lookup_type = 'MTL_TRANSACTION_ACTION'
       AND flv.lookup_code = mtt.transaction_action_id
       AND mtxs.transaction_source_type_id = mmt.transaction_source_type_id
       AND msi.segment1 LIKE '1-FG%'
       AND mmt.inventory_item_id = cog.inventory_item_id(+)
       AND mmt.organization_id = cog.organization_id(+)
       AND TO_CHAR ( mmt.transaction_date, 'MON-RR') = cog.periode_name(+)