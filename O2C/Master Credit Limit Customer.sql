/* Formatted on 11/28/2017 4:59:24 PM (QP5 v5.256.13226.35510) */
  SELECT cust.cust_account_id customer_account_id
       , cust.account_number customer_number
       , party.party_name customer_name
       , cust.status
       , site.location bill_to_location
       , acct.status site_status
       , hcpa.currency_code
       , hcpa.overall_credit_limit
       , hcp.creation_date
       , hcp.last_update_date
    FROM hz_cust_accounts cust
       , hz_cust_acct_sites_all acct
       , hz_cust_site_uses_all site
       , hz_party_sites party_site
       , hz_locations loc
       , hz_parties party
       , hz_customer_profiles hcp
       , hz_cust_profile_amts hcpa
   WHERE     cust.cust_account_id = acct.cust_account_id
         AND acct.cust_acct_site_id = site.cust_acct_site_id
         AND site.site_use_code = 'BILL_TO'
         AND acct.org_id = site.org_id
         AND loc.location_id = party_site.location_id
         AND acct.party_site_id = party_site.party_site_id
         AND cust.party_id = party.party_id
         AND hcp.party_id = party.party_id
         AND cust.cust_account_id = hcp.cust_account_id
         AND hcp.cust_account_profile_id = hcpa.cust_account_profile_id
         AND hcp.site_use_id = site.site_use_id
ORDER BY cust.account_number, site_use_code