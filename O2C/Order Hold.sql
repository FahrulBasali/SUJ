SELECT oeh.order_number
     , oeh.header_id
     , ohde.name hold_name
     , oho.creation_date
     , oho.last_update_date
  FROM oe_order_holds_all oho
     , oe_order_headers_all oeh
     , oe_hold_sources_all os
     , oe_hold_definitions ohde
WHERE oho.released_flag = 'N' AND oeh.header_id = oho.header_id AND oho.hold_source_id = os.hold_source_Id AND os.hold_id = ohde.hold_id;
