SELECT wmd.delivery_id
       , mp.organization_code inv_organization
       , wmd.attribute1 po_ekepedisi
       , wmd.attribute2 po_line_number
       , pla.line_location_id
       , poh.creation_date po_date
       , pol.quantity
       , pla.quantity_billed
       , pol.unit_price
       , poh.currency_code
       , poh.rate
       , pol.unit_price * pol.quantity po_amount
       , NVL (poh.rate, 1) * pol.unit_price * pol.quantity po_amount_func
       , pol.unit_price * pla.quantity_billed billed_amount
    FROM mtl_parameters mp
       , wsh_new_deliveries wmd
       , po_headers_all poh
       , po_lines_all pol
       , po_line_Locations_all pla
   WHERE     mp.organization_id = wmd.organization_id
         AND poh.segment1 = wmd.attribute1
         AND pol.line_num = wmd.attribute2
         AND poh.po_Header_Id = pol.po_Header_id
         AND pol.po_header_id = pla.po_header_Id
         AND pol.po_line_id = pla.po_line_Id
         AND EXISTS
                (SELECT 1
                   FROM wsh_delivery_assignments wda
                  WHERE wmd.delivery_id = wda.delivery_id);


