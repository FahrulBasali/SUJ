/* Formatted on 11/28/2017 2:22:51 PM (QP5 v5.256.13226.35510) */
SELECT mtrh.request_number mo_reference_number
     , mtrh.header_id mo_reference_id
     , k.request_number mo_alih_number
     , k.header_id mo_alih_id
     , k.line_number line_number_alih
     , k.transaction_id
     , k.transaction_date
     , k.transaction_quantity
     , k.transaction_uom
     , mp1.organization_code source_organization
     , k.transfer_subinventory source_subinventory
     , k.subinventory_code destination_subinventory
     , mp2.organization_code destination_organization
     , harga_so.currency_code
     , harga_so.rate_idr_usd
     , harga_so.rate_usd_idr
     , harga_so.price_idr
     , harga_so.price_usd
     , harga_so.price_idr * k.transaction_quantity amount_idr
     , harga_so.price_usd * k.transaction_quantity amount_usd
  FROM mtl_txn_request_lines mtrl
     , mtl_txn_request_headers mtrh
     , mtl_material_transactions mmt2
     , (SELECT mtrl.attribute5
             , mmt.transaction_quantity
             , mtrh.request_number
             , mtrh.header_id
             , mmt.transaction_id
             , mmt.transaction_date
             , mtrl.line_number
             , mtrh.attribute1
             , mmt.organization_id
             , mmt.subinventory_code
             , mmt.transfer_subinventory
             , mmt.transaction_uom
          FROM mtl_txn_request_lines mtrl, mtl_txn_request_headers mtrh, mtl_material_transactions mmt
         WHERE     1 = 1
               AND mtrl.header_id = mtrh.header_id
               AND mmt.move_order_line_id = mtrl.line_id
               AND mtrh.from_subinventory_code IN ('WHS_ALIH', 'WHS_ALIH_A', 'WHS_ALIH_B')
               AND mmt.subinventory_code = 'WHS_STM'
               AND mtrl.attribute5 IS NOT NULL
               AND mtrl.attribute_category = 'Move Order Moving') k
     , mtl_parameters mp1
     , mtl_parameters mp2
     , (  SELECT mvl.header_id mo_reference_id
               , oeh.transactional_curr_code currency_code
               , MAX ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'IDR', 'USD') END)
                    rate_idr_usd
               , MAX ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END)
                    rate_usd_idr
               , AVG ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END * oel.unit_selling_price)
                    price_idr
               , AVG ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE 1 END * oel.unit_selling_price) price_usd
            FROM mtl_material_transactions mmt
               , mtl_txn_request_lines mvl
               , oe_order_lines_all oel
               , oe_order_headers_all oeh
               , gl_daily_rates_v gdr
           WHERE     mmt.move_order_line_id = mvl.line_id
                 AND mmt.trx_source_line_id = oel.line_id
                 AND oel.header_id = oeh.header_id
                 AND mmt.subinventory_code = 'WHS_STG'
                 AND oeh.transactional_curr_code = gdr.from_currency(+)
                 AND gdr.from_currency(+) = 'IDR'
                 AND gdr.to_currency(+) = 'USD'
                 AND oeh.conversion_type_code = gdr.conversion_type(+)
                 AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
        GROUP BY mvl.header_id, oeh.transactional_curr_code) harga_so
 WHERE     1 = 1
       AND mtrl.header_id = mtrh.header_id
       AND mmt2.move_order_line_id = mtrl.line_id
       AND mtrh.request_number = k.attribute5
       AND SUBSTR ( mmt2.subinventory_code, 1, 10) <> 'WHS_STG'
       AND mp1.organization_id = k.organization_id
       AND mp2.organization_id = k.attribute1
       AND harga_so.mo_reference_id = mtrh.header_id
--Dari subinventory staging transfer ke subinventory alih , dari alih transfer ke subinventory SMT untuk transfer ke organization lain, pada saat transfer ke SMT akan diassign nomor mo lama di line move order pick nya