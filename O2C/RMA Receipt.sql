/* Formatted on 11/28/2017 2:50:00 PM (QP5 v5.256.13226.35510) */
 -- Query RMA

SELECT NVL (ol.blanket_number, oh.blanket_number) sales_agreement_number
     , NVL (oobha.sales_document_name, oobha2.sales_document_name) sales_agreement_name
     , oh.order_number rma_number
     , oh.header_id rma_id
     , pl.name price_list_name
     , oh.price_list_id
     , mp.organization_code inventory_organization
     , rt.subinventory
     , ol.ordered_item item
     , mmt.transaction_id
     , mmt.transaction_quantity
     , mmt.transaction_uom uom
     , oh.transactional_curr_code currency_code
     , ol.unit_selling_price
     , CASE
          WHEN oh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate
          ELSE suj_get_closing_rate ( NVL (TRUNC ( oh.conversion_rate_date), TRUNC ( oh.request_date)), 'IDR', 'USD')
       END
          rate_idr_usd
     , CASE
          WHEN oh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate
          ELSE suj_get_closing_rate ( NVL (TRUNC ( oh.conversion_rate_date), TRUNC ( oh.request_date)), 'USD', 'IDR')
       END
          rate_usd_idr
     ,   CASE
            WHEN oh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate
            ELSE suj_get_closing_rate ( NVL (TRUNC ( oh.conversion_rate_date), TRUNC ( oh.request_date)), 'USD', 'IDR')
         END
       * ol.unit_selling_price
       * mmt.transaction_quantity
          amount_idr
     ,   CASE
            WHEN oh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate
            ELSE suj_get_closing_rate ( NVL (TRUNC ( oh.conversion_rate_date), TRUNC ( oh.request_date)), 'IDR', 'USD')
         END
       * ol.unit_selling_price
       * mmt.transaction_quantity
          amount_usd
     , mmt.creation_date
     , mmt.last_update_date
  FROM mtl_material_transactions mmt
     , rcv_transactions rt
     , oe_order_lines_all ol
     , oe_order_headers_all oh
     , qp_list_headers_v pl
     , mtl_parameters mp
     , oe_blanket_headers_all oobha
     , oe_blanket_headers_all oobha2
     , (SELECT gdr.conversion_type
             , gdr.conversion_rate
             , gdr.conversion_date
             , gdr.from_currency
             , gdr.to_currency
             , gdr.inverse_conversion_rate
          FROM gl_daily_rates_v gdr, gl_sets_of_books sob
         WHERE     sob.currency_code = gdr.to_currency
               AND sob.set_of_books_id = (SELECT profile_option_value
                                            FROM fnd_profile_option_values pv, fnd_profile_options po
                                           WHERE po.profile_option_id = pv.profile_option_id AND po.profile_option_name = 'GL_SET_OF_BKS_ID' AND pv.level_id = 10001)) gdr
 WHERE     rt.source_document_code = 'RMA'
       AND rt.destination_type_code = 'INVENTORY'
       AND ol.line_id = rt.oe_order_line_id
       AND oh.header_id = rt.oe_order_header_id
       AND pl.list_header_id(+) = oh.price_list_id
       AND rt.organization_id = mp.organization_id
       AND mmt.organization_id = rt.organization_id
       AND mmt.rcv_transaction_id = rt.transaction_id
       AND ol.blanket_number = oobha.order_number(+)
       AND oh.blanket_number = oobha2.order_number(+)
       AND oh.transactional_curr_code = gdr.from_currency(+)
       AND oh.conversion_type_code = gdr.conversion_type(+)
       AND NVL (TRUNC ( oh.conversion_rate_date), TRUNC ( oh.request_date)) = TRUNC ( gdr.conversion_date(+));