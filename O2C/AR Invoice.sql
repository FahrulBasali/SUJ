/* Formatted on 11/27/2017 3:26:48 PM (QP5 v5.256.13226.35510) */
/* Query Invoice Receivables */
-- Invoice OM
SELECT mvl.line_id mo_line_id
     , ooh.order_number
     , ooh.header_id so_header_id
     , mvh.request_number mo_number
     , mvh.header_id mo_header_id
     , wda.delivery_id
     , rct.trx_number invoice_number
     , rctype.TYPE
     , absa.name source
     , rct.customer_trx_id
     , rctype.name invoice_type
     , rct.creation_date invoice_creation_date
     , rct.trx_date invoice_date
     , gd.gl_date
     , rct.attribute1 invoice_send_date
     , NVL (rct.term_due_date, apsa.due_date) invoice_due_date
     , ratt.name payment_term
     , rctl.line_number invoice_line
     , rctl.customer_trx_line_id invoice_line_id
     , wdd.shipped_quantity invoice_quantity
     , rctl.unit_selling_price
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'IDR', 'USD') END rate_idr_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 / rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END rate_usd_idr
     , rct.invoice_currency_code
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN wdd.shipped_quantity * rctl.unit_selling_price * rct.exchange_rate ELSE wdd.shipped_quantity * rctl.unit_selling_price END
          invoice_amount_usd
     , CASE
          WHEN rct.invoice_currency_code = 'IDR' THEN wdd.shipped_quantity * rctl.unit_selling_price
          ELSE wdd.shipped_quantity * rctl.unit_selling_price * suj_get_closing_rate ( rct.trx_date, 'USD', 'IDR')
       END
          invoice_amount_idr
     ,   (rctl.tax_recoverable / rctl.extended_amount)
       * (wdd.shipped_quantity * rctl.unit_selling_price)
       * CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE 1 END
          tax_recoverable_usd
     ,   (rctl.tax_recoverable / rctl.extended_amount)
       * (wdd.shipped_quantity * rctl.unit_selling_price)
       * CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END
          tax_recoverable_idr
     , rct.creation_date header_creation_date
     , rct.last_update_date header_last_update_date
     , rctl.creation_date line_creation_date
     , rctl.last_update_date line_last_update_date
     , rctl.interface_line_context
  FROM oe_order_headers_all ooh
     , oe_order_lines_all ool
     , wsh_delivery_details wdd
     , wsh_new_deliveries wnd
     , wsh_delivery_assignments wda
     , ra_customer_trx_all rct
     , ra_customer_trx_lines_all rctl
     , mtl_txn_request_lines mvl
     , mtl_txn_request_headers mvh
     , ra_cust_trx_types_all rctype
     , ra_terms_tl ratt
     , ar_payment_schedules_all apsa
     , ra_cust_trx_line_gl_dist_all gd
     , ra_batch_sources_all absa
 WHERE     ooh.header_id = ool.header_id
       AND wdd.source_header_id = ooh.header_id
       AND wdd.delivery_detail_id = wda.delivery_detail_id
       AND wda.delivery_id = wnd.delivery_id
       AND rctl.interface_line_attribute1 = TO_CHAR ( ooh.order_number)
       AND rctl.interface_line_attribute6 = TO_CHAR ( ool.line_id)
       AND rctl.interface_line_attribute3 = TO_CHAR ( wnd.delivery_id)
       AND rctl.customer_trx_id = rct.customer_trx_id
       AND rctl.interface_line_context = 'ORDER ENTRY'
       AND mvl.header_id = mvh.header_id
       AND wdd.move_order_line_id = mvl.line_id
       AND rctype.cust_trx_type_id = rct.cust_trx_type_id
       AND ratt.term_id(+) = rct.term_id
       AND ratt.language(+) = USERENV ( 'LANG')
       AND wdd.source_line_id = ool.line_id
       AND rctype.TYPE = 'INV'
       AND rct.customer_trx_id = apsa.customer_trx_id(+)
       AND 'REC' = gd.account_class
       AND 'Y' = gd.latest_rec_flag
       AND rct.customer_trx_id = gd.customer_trx_id
       AND absa.batch_source_id = rct.batch_source_id
       AND rctl.line_type <> 'TAX'
UNION ALL
-- Non Order Entry
SELECT NULL mo_line_id
     , NULL order_number
     , NULL so_header_id
     , NULL mo_number
     , NULL mo_header_id
     , NULL delivery_id
     , rct.trx_number invoice_number
     , rctype.TYPE
     , absa.name source
     , rct.customer_trx_id
     , rctype.name invoice_type
     , rct.creation_date invoice_creation_date
     , rct.trx_date invoice_date
     , gd.gl_date
     , rct.attribute1 invoice_send_date
     , NVL (rct.term_due_date, apsa.due_date) invoice_due_date
     , ratt.name payment_term
     , rctl.line_number invoice_line
     , rctl.customer_trx_line_id invoice_line_id
     , rctl.quantity_invoiced invoice_quantity
     , rctl.unit_selling_price
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'IDR', 'USD') END rate_idr_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 / rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END rate_usd_idr
     , rct.invoice_currency_code
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rctl.extended_amount * rct.exchange_rate ELSE rctl.extended_amount END invoice_amount_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rctl.extended_amount ELSE rctl.extended_amount * suj_get_closing_rate ( rct.trx_date, 'USD', 'IDR') END invoice_amount_idr
     , (rctl.tax_recoverable) * CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE 1 END tax_recoverable_usd
     , (rctl.tax_recoverable) * CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END tax_recoverable_idr
     , rct.creation_date header_creation_date
     , rct.last_update_date header_last_update_date
     , rctl.creation_date line_creation_date
     , rctl.last_update_date line_last_update_date
     , rctl.interface_line_context
  FROM ra_customer_trx_all rct
     , ra_customer_trx_lines_all rctl
     , ra_cust_trx_types_all rctype
     , ra_terms_tl ratt
     , ar_payment_schedules_all apsa
     , ra_cust_trx_line_gl_dist_all gd
     , ra_batch_sources_all absa
 WHERE     rctl.customer_trx_id = rct.customer_trx_id
       AND (rctl.interface_line_context <> 'ORDER ENTRY' OR rctl.interface_line_context IS NULL)
       AND rctype.cust_trx_type_id = rct.cust_trx_type_id
       AND ratt.term_id(+) = rct.term_id
       AND ratt.language(+) = USERENV ( 'LANG')
       AND rct.customer_trx_id = apsa.customer_trx_id(+)
       AND 'REC' = gd.account_class
       AND 'Y' = gd.latest_rec_flag
       AND rct.customer_trx_id = gd.customer_trx_id
       AND absa.batch_source_id = rct.batch_source_id
       AND rctl.line_type <> 'TAX'
UNION ALL
-- So to Credit Memo
SELECT NULL mo_line_id
     , ooh.order_number
     , ooh.header_id so_header_id
     , NULL mo_number
     , NULL mo_header_id
     , NULL delivery_id
     , rct.trx_number invoice_number
     , rctype.TYPE
     , absa.name source
     , rct.customer_trx_id
     , rctype.name invoice_type
     , rct.creation_date invoice_creation_date
     , rct.trx_date invoice_date
     , gd.gl_date
     , rct.attribute1 invoice_send_date
     , NVL (rct.term_due_date, apsa.due_date) invoice_due_date
     , ratt.name payment_term
     , rctl.line_number invoice_line
     , rctl.customer_trx_line_id invoice_line_id
     , rctl.quantity_invoiced invoice_quantity
     , rctl.unit_selling_price
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'IDR', 'USD') END rate_idr_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 / rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END rate_usd_idr
     , rct.invoice_currency_code
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rctl.extended_amount * rct.exchange_rate ELSE rctl.extended_amount END invoice_amount_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rctl.extended_amount ELSE rctl.extended_amount * suj_get_closing_rate ( rct.trx_date, 'USD', 'IDR') END invoice_amount_idr
     , (rctl.tax_recoverable) * CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE 1 END tax_recoverable_usd
     , (rctl.tax_recoverable) * CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END tax_recoverable_idr
     , rct.creation_date header_creation_date
     , rct.last_update_date header_last_update_date
     , rctl.creation_date line_creation_date
     , rctl.last_update_date line_last_update_date
     , rctl.interface_line_context
  FROM ra_customer_trx_all rct
     , ra_customer_trx_lines_all rctl
     , ra_cust_trx_types_all rctype
     , ra_terms_tl ratt
     , oe_order_headers_all ooh
     , oe_order_lines_all ool
     , ar_payment_schedules_all apsa
     , ra_cust_trx_line_gl_dist_all gd
     , ra_batch_sources_all absa
 WHERE     rctl.customer_trx_id = rct.customer_trx_id
       AND rctl.interface_line_context = 'ORDER ENTRY'
       AND rctype.cust_trx_type_id = rct.cust_trx_type_id
       AND ratt.term_id(+) = rct.term_id
       AND ratt.language(+) = USERENV ( 'LANG')
       AND rctype.TYPE = 'CM'
       AND rctl.interface_line_attribute1 = ooh.order_number
       AND rctl.interface_line_attribute6 = TO_CHAR ( ool.line_id)
       AND rct.customer_trx_id = apsa.customer_trx_id(+)
       AND 'REC' = gd.account_class
       AND 'Y' = gd.latest_rec_flag
       AND rct.customer_trx_id = gd.customer_trx_id
       AND absa.batch_source_id = rct.batch_source_id
       AND rctl.line_type <> 'TAX';